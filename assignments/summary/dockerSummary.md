# Docker Summary

## What is Docker?

Docker is a platform that enables you to develop, ship, and run applications as containers. The Docker Daemon process (or engine) runs on your host OS which manages images and containers.

There are some terms that you must know:  

* **Dockerfile** is a text document that contains all the commands you would normally execute  
  manually in order to build a Docker image.  

* **Docker Images** are the basis of containers. An Image is an ordered collection of root  
  filesystem changes.  

* **Docker Container** is a runtime instance of a docker image.  

* **Dockerhub** is a service provided by Docker for finding and sharing container images with your team.  
  It is similar to github but only for docker image and container.

![dockerfiletocontainer](/uploads/99579b9bc29a6c413905032eb6b79ad5/dockerfiletocontainer.png)

 
## Dcoker Commands

There are some basic commands:  

* **docker --version** - This command is used to get the currently installed version of docker.  
* **docker ps** - This command is used to list the running containers.  
* **docker ps-a** -This command is used to show all the running and exited containers.  
* **docker start** - Start one or more stopped containers.  
  `docker start [OPTIONS] CONTAINER [CONTAINER...]`  
* **docker stop** - This command stops a running container.  
    `Usage: docker stop <container id>` 

* **docker exec** - This command is used to access the running container.  
    `Usage: docker exec -it <container id> bash`

* **docker kill** - This command kills the container by stopping its execution immediately.    
  `Usage: docker kill <container id>`  

* **docker commit** - This command creates a new image of an edited container on the local system.  
  `Usage: docker commit <conatainer id> <username/imagename>`  

* **docker images** - This command lists all the locally stored docker images.
* **docker rm** - This command is used to delete a stopped container  
  `Usage: docker rm <container id>`  

* **docker rmi** - This command is used to delete an image from local storage.  
  `Usage: docker rmi <image-id>`  

* **docker build** - This command is used to build an image from a specified docker file.  
  `Usage: docker build <path to docker file>`  

* **docker run** - This command is used to create a container from an image.  
  `Usage: docker run -it -d <image name>`  

* **docker push** - This command is used to push an image to the docker hub repository.  
  `Usage: docker push <username/image name>`  

* **docker pull** - This command is used to pull images from the docker repository.  
  `Usage: docker pull <image name>`


![0_pdwNqCvWJYAsGunP](/uploads/3c31b89e506d0505c5f5cf92bae87835/0_pdwNqCvWJYAsGunP.jpg)




 









  









