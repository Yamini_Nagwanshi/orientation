# Git summary

Git is a distributed version-control system for tracking changes in source code during software development. It was created by Linus Torvalds.
It is designed for coordinating work among programmers, but it can be used to track changes in any set of files.

In order to install Git:
  For Linux, open terminal and write
    - sudo apt-get install git 
  For Window, open url and download git
    - [](https://git-scm.com/)

There are three steps:
    - Workspace : Your working directories, It contains lists of file names and reference 
    - Stage :  Allows to continue making changes to working directories.
    - Commit : It is used for saving changes. 

There are some main operations :

1- **Fork** is a copy of a repository that allows you to freely experiment with changes
  without affecting the original project.

![fork](/uploads/8e4ff871ebb13eedf30ac82a0575f01b/fork.jpg)
    
2- **Clone** is a Git command line utility which is used to target an existing repository and create a clone, or copy of 
  the target repository.
  As you copy the https link for cloning repository.

![clone](/uploads/31a4436db123dfec65f8279d59848019/clone.jpg)

Open git terminal in your local folder then use git command with copied https link.

![gitclone](/uploads/d17ca1e458b83207962531220180d143/gitclone.png)


3- **Branch** in Git is simply a lightweight movable pointer to one of these commits. In order to check status of branch and to create new branch. 

![branch](/uploads/ed9366fa32cddbe13edce8da37b6a0df/branch.png)

4- **git add** command adds a change in the working directory to the staging area. It tells Git that you want to include updates to a particular file in the next commit.

![add](/uploads/1cb49632c788526067de40d2d821924d/add.png)

5- **Commit** is the term used for saving changes. Git commit command is used with a message indicating what change we made:

![commit](/uploads/d895a52b0c7e1bc76a51a79f941828a0/commit.png)

6- The **git push** command is used to upload local repository content to a remote repository. Before using "git push", make sure the correct local branch is checked out. Then, to perform the push, simply specify which remote branch you want to push to:

![push](/uploads/2021441a8fe4bdc34d30a3cc2b44a84f/push.png)

7- The **git merge** command lets you take the independent lines of development created by git branch and integrate them into a single branch. Before  actually starting merging branches, make sure you are switched to main branch where we want to merge.

![merge](/uploads/a4971c96423800178c99305a2080d182/merge.png)
